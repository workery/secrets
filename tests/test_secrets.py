from os import getenv
from typing import Self
from workery_secrets import (
	Secret
)


class TestSecrets:
	def test_hash_value_as_bytes(
		self: Self,
		secret: Secret,
		passphrase: str
	) -> bool:
		assert isinstance(
			secret.encrypt(
				passphrase.encode()
			),
			str
		)

	def test_hash_value_as_string(
		self: Self,
		secret: Secret,
		passphrase: str
	) -> bool:
		assert isinstance(
			secret.encrypt(
				passphrase
			),
			str
		)

	def test_decrypt_passphrase(
		self: Self,
		secret: Secret,
		passphrase: str
	) -> bool:
		assert secret.decrypt(
			secret.encrypt(
				passphrase
			)
		) == passphrase

	def test_hash_passphrase(
		self: Self,
		secret: Secret,
		passphrase: str
	) -> bool:
		assert isinstance(
			secret.mess(passphrase),
			str
		)

	def test_verify_passphrase_hash(
		self: Self,
		secret: Secret,
		passphrase: str
	) -> bool:
		assert secret.verify(
			secret.mess(
				passphrase
			),
			passphrase
		)


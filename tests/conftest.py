from cryptography.fernet import Fernet
from pytest import fixture
from workery_secrets import Secret


@fixture(scope='session')
def secret():
	return Secret(
		Fernet.generate_key()
	)

@fixture(scope='session')
def passphrase():
	return 'Test string'

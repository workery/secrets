from argon2 import PasswordHasher
from cryptography.fernet import (
	Fernet
)
from typing import Self, Union


class Secret:

	def __init__(
		self: Self,
		key: Union[str, bytes]
	) -> None:
		self.key = key
		self.__crypt = Fernet(self.key)
		self.__hasher = PasswordHasher()

	def encrypt(
		self: Self,
		value: Union[str, bytes]
	) -> str:
		return self.__crypt.encrypt(
			value if isinstance(
				value,
				bytes
			) else value.encode()
		).decode()

	def decrypt(
		self: Self,
		value: str
	) -> str:
		return self.__crypt.decrypt(
			value if isinstance(
				value,
				bytes
			) else value.encode()
		).decode()

	def mess(
		self: Self,
		value: str
	) -> str:
		return self.__hasher.hash(
			str(value)
		)

	def verify(
		self: Self,
		hash: str,
		value: str
	) -> bool:
		return self.__hasher.verify(
			hash, value
		)
from cryptography.fernet import Fernet
from dotenv import load_dotenv
from os import getenv
from rich import print
from typing import Self
from workery_secrets import Secret


load_dotenv()


class SecretCli:
	__secret = secret = Secret(
		getenv(
			'WORKERY_SECRET'
		) or Fernet.generate_key()
	)
	def __init__(
		self: Self,
		*args, **kwargs
	) -> None:
		command = args[0][0]
		match command:
			case 'encrypt':
				print(
					self.__secret.encrypt(
						args[0][1]
					)
				)
			case 'decrypt':
				print(
					self.__secret.decrypt(
						args[0][1]
					)
				)
			case 'mess':
				print(
					self.__secret.mess(
						args[0][1]
					)
				)
			case 'verify':
				print(
					self.__secret.verify(
						args[0][1],
						args[0][2]
					)
				)
			case 'help':
				self.help
			case _:
				print(
					f'\n[bold yellow on red]Command [blink2]{ command }[/blink2] not found![bold yellow on red]'
				)
				self.help

	@property
	def help(self: Self) -> str:
		print(
			'\nOptions:\n' +\
			'\tencrypt <value>\n' +\
			'\tdecrypt <hash>\n' +\
			'\tmess <value>\n' +\
			'\tverify <hash> <value>\n'
		)

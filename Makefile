html:
	coverage html
debug:
	python -i debug.py
pdb:
	pytest --pdb
report:
	coverage report -m
test:
	coverage run --module pytest --tui --verbose --capture=no
	coverage report -m
	coverage html

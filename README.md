# Workery Secrets


Usage example:
```python

>>> from cryptography.fernet import Fernet
>>> from workery_secrets import Secret
>>>
>>>
>>> secret = Secret(key=Fernet.generate_key())
>>> passphrase = 'X7r3m3Ly$7r0ngp4$5w0rD4N07h1n6'
>>> encrypted_passphrase = secret.encrypt(
>>>     passphrase
>>> )
>>> encrypted_passphrase
'gAAAAABk5mVL0cv_HjlKgVVUILPikMExwBXdP-bbfbaZ7IENaC58YqS0aBXOIYsb2anQgXE6C7qLMlNf_K_-HFzsv5tFxv-VgMQA68YtJrv6URMlZ-S0hT8='
>>> secret.decrypt(encrypted_passphrase)
'X7r3m3Ly$7r0ngp4$5w0rD4N07h1n6'
>>> mess = secret.mess(passphrase)
>>> mess
'$argon2id$v=19$m=65536,t=3,p=4$bY8aEwQ1voOdGjx6v0eoBw$oICI/rJcMBOCqOLHu1DkDbbJYrE8DA0EQ/b2ldbcyNU'
>>> secret.verify(mess, passphrase)
True
```
If you want to use it in a script:
```bash
$ python -m workery_secrets help
```
Commands are:

* encrypt [value]
* decrypt [hash]
* mess [value]
* verify [hash] [value]

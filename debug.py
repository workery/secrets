from cryptography.fernet import Fernet
from workery_secrets import Secret


def key():
	return Fernet.generate_key()

secret = Secret(Fernet.generate_key())


def mcrypt(value: str, steps: int) -> str:
	key = value
	for step in range(steps):
		key = secret.encrypt(key)
	return key

def mdecrypt(value: str, steps: int) -> str:
	key = value
	for step in range(steps):
		key = secret.decrypt(key)
	return key